FROM rust:latest as builder

WORKDIR /usr/src/telegram-chess-bot
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y openssl ca-certificates && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/telegram-chess-bot /usr/local/bin/telegram-chess-bot
CMD ["telegram-chess-bot"]
